 #include <iostream>
 #include <string>
 #include "DynamicQueue.h"
 using namespace std;

int queue_size;

 int main()
{
string name;

cout << "Enter the size of queue" << endl;
cin >> queue_size;

while(queue_size < 1){
    cout << "Invalid input" << endl;
    
    cout << "Enter the size of queue" << endl;
    cin >> queue_size;
}


DynamicQueue<string> queue;

cin.ignore();

for (int count = 0; count < queue_size; count++)
{

cout << "Enter a name: ";
getline(cin, name);
queue.enqueue(name);
}


cout << endl << "Here are the names you entered:" << endl;
for (int count = 0; count < queue_size; count++)
{
queue.dequeue(name);
cout << name << endl;
}
return 0;
}