#ifndef DYNAMICQUEUE_H
#define DYNAMICQUEUE_H
#include <iostream>
using namespace std;
template <class T>
class DynamicQueue
{
private:
struct QueueNode
{
T value; 
QueueNode *next;
};
QueueNode *front; 
QueueNode *rear; 
int numItems; 
public:
DynamicQueue();
~DynamicQueue();


void enqueue(T);
void dequeue(T &);
bool isEmpty() const;
bool isFull() const;
void clear();
};

template <class T>
DynamicQueue<T>::DynamicQueue()
{
front = NULL;
rear = NULL;
numItems = 0;
}
template <class T>
DynamicQueue<T>::~DynamicQueue()
{
clear();
}


template <class T>
void DynamicQueue<T>::enqueue(T item)
{
QueueNode *newNode = NULL;

newNode = new QueueNode;
newNode->value = item;
newNode->next = NULL;

if (isEmpty())
{
front = newNode;
rear = newNode;
}
else
{
rear->next = newNode;
rear = newNode;
}

numItems++;
}

template <class T>
void DynamicQueue<T>::dequeue(T &item)
{
QueueNode *temp = NULL;

if (isEmpty())
{
cout << "The queue is empty." << endl;
}
else
{

item = front->value;

temp = front;
front = front->next;
delete temp;


numItems--;
}
}

 
template <class T>
bool DynamicQueue<T>::isEmpty() const
{
bool status;

if (numItems > 0)
status = false;
else
status = true;
return status;
}

template <class T>
void DynamicQueue<T>::clear()
{
T value;

while(!isEmpty())
dequeue(value);
}
#endif 