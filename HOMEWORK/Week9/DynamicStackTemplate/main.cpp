
  #include <iostream>
  #include <string>
  #include "DynamicStack.h"
  using namespace std;
 
  int main()
 {
 int stackSize; 
 int choice; 
 string item;
 
 
 cout << "Enter stack size" << endl;
 cin >> stackSize;
 
 while (stackSize < 1)
{
cout << "Enter 1 or greater: ";
cin >> stackSize;
}
 
 
DynamicStack<string> stack;
 do
 {
     
     cout << "Press 1 to add to stack" << endl;    
     cout << "Press 2 to remove from stack" << endl; 
     cout << "Press 3 to end" << endl; 
     
     cin >> choice;
     
     while (choice < 1 || choice > 3){
         
         cout << "Invalid input" << endl;
         
         cout << "Press 1 to add to stack" << endl;    
         cout << "Press 2 to remove from stack" << endl; 
         cout << "Press 3 to end" << endl; 
     
     cin >> choice;
     }
     
     if(choice == 1){
  
        cin.ignore();
        cout << "Enter an item: " << endl;;
        getline(cin, item);
        stack.push(item);
        }
     
     if(choice == 2){
         
        item = "";
        stack.pop(item);
        if (item != "")
        cout << item << " was popped" << endl;;
              
        }
     

 } while (choice != 3);
  return 0; 
}
