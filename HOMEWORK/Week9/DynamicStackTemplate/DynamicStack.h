#ifndef DYNAMICSTACK_H
#define DYNAMICSTACK_H
#include <iostream>
using namespace std;
// Stack template
template <class T>
class DynamicStack
{
private:
// Structure for the stack nodes
struct StackNode
{
T value; // Value in the node
StackNode *next; // Pointer to the next node
}; 


StackNode *top;
public:

DynamicStack()
{ top = NULL; }


~DynamicStack();

void push(T);
void pop(T &);
bool isEmpty();
};


template <class T> 
DynamicStack<T>::~DynamicStack()
{
StackNode *nodePtr, *nextNode;


nodePtr = top;

while (nodePtr != NULL)
{
nextNode = nodePtr->next;
delete nodePtr;
nodePtr = nextNode;
}
}


template <class T>
void DynamicStack<T>::push(T item)
{
StackNode *newNode = NULL; 

newNode = new StackNode;
newNode->value = item;



if (isEmpty()) 
{
top = newNode;
newNode->next = NULL;
}
else 
{
newNode->next = top;
top = newNode;
}
}


template <class T>
void DynamicStack<T>::pop(T &item)
{
StackNode *temp = NULL; 

if (isEmpty())
{
cout << "The stack is empty." << endl;
}
else 
{
item = top->value;
temp = top->next;
delete top;
top = temp;
}
}


template <class T>
bool DynamicStack<T>::isEmpty()
{
bool status;

if (!top){
status; }
else{
status = false;}
return status;
}
#endif 