#ifndef DYNAMICQUEUE_H
#define DYNAMICQUEUE_H
#include <iostream>
#include <new>
using namespace std;
template <class T>
class DynamicQueue
{
private:
T *aptr;
int arraySize;
void memError();
void subError();
struct QueueNode
{
T value; 
QueueNode *next;
};
QueueNode *front; 
QueueNode *rear; 
int numItems; 
public:
DynamicQueue(int);
~DynamicQueue();


void enqueue(T);
void dequeue(T &);
bool isEmpty() const;
bool isFull() const;
void clear();
};

template <class T>
DynamicQueue<T>::DynamicQueue(int s)
{
    
 arraySize = s;
 try
 {
 aptr = new T [s];
 }
 catch (bad_alloc)
 {
 memError();
 } 
 
front = NULL;
rear = NULL;
numItems = 0;
}
template <class T>
DynamicQueue<T>::~DynamicQueue()
{
clear();
}


template <class T>
void DynamicQueue<T>::enqueue(T item)
{
QueueNode *newNode = NULL;

newNode = new QueueNode;
newNode->value = item;
newNode->next = NULL;

if (isEmpty())
{
front = newNode;
rear = newNode;
}
else
{
rear->next = newNode;
rear = newNode;
}

numItems++;
}

template <class T>
void DynamicQueue<T>::dequeue(T &item)
{
QueueNode *temp = NULL;

if (isEmpty())
{
cout << "The queue is empty." << endl;
}
else
{

item = front->value;

temp = front;
front = front->next;
delete temp;


numItems--;
}
}

 
template <class T>
bool DynamicQueue<T>::isEmpty() const
{
bool status;

if (numItems > 0)
status = false;
else
status = true;
return status;
}

template <class T>
void DynamicQueue<T>::clear()
{
T value;

while(!isEmpty())
dequeue(value);
}

template <class T>
void DynamicQueue<T>::memError()
{
cout << "ERROR:Cannot allocate memory.\n";
exit(EXIT_FAILURE);
}


template <class T>
void DynamicQueue<T>::subError()
{
cout << "ERROR: Subscript out of range.\n";
exit(EXIT_FAILURE);
} 
#endif 