

#include "DynamicStringStack.h"
#include <iostream>

using namespace std;

DynamicStringStack::~DynamicStringStack()
{
StackNode *nodePtr = NULL, *nextNode = NULL;

nodePtr = top;

while (nodePtr != NULL)
{
nextNode = nodePtr->next;
delete nodePtr;
nodePtr = nextNode;
}
}

 void DynamicStringStack::push(string num)
{
StackNode *newNode = NULL; 

newNode = new StackNode;
newNode->value = num;

if (isEmpty())
{
top = newNode;
newNode->next = NULL;
}
else
{
newNode->next = top;
top = newNode;
}
}

void DynamicStringStack::pop(string &num)
{ 
StackNode *temp = NULL; 

if (isEmpty())
{
cout << "The stack is empty." << endl;
}
else  
{
num = top->value;
temp = top->next;
delete top;
top = temp;
}
}

bool DynamicStringStack::isEmpty()
{
bool status;

if (!top)
status = true;
else
status = false;

return status;
}