
#ifndef DYNAMICSTRINGSTACK_H
#define DYNAMICSTRINGSTACK_H
#include <cstdlib> 
#include <string>
using namespace std;


class DynamicStringStack
{
private:
struct StackNode
{
string value; 
StackNode *next; 
};

StackNode *top;

public:

DynamicStringStack()
{ top = NULL; }
~DynamicStringStack();


void push(string);
void pop(string &);
bool isEmpty();
};
#endif
