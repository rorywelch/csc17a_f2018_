/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Coin.cpp
 * Author: s
 * 
 * Created on October 7, 2018, 11:03 PM
 */

#include "Coin.h"
#include <string>
#include <ctime>
#include <cstdlib>
#include <iostream>

using namespace std;

Coin::Coin() 
{
    unsigned seed = time(0);
    srand(seed);
    
    headsTails = (rand() % ( MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    
    
    if (headsTails == 1)
    {
        sideUp = "heads";
    }
    else if (headsTails == 2)
    {
        sideUp = "tails";
    }
    
}

Coin::Coin(const Coin& orig) {
}

Coin::~Coin() {
}

void Coin::toss()
{
    
    headsTails = (rand() % ( MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    
    
    if (headsTails == 1)
    {
        sideUp = "heads";
    }
    else if (headsTails == 2)
    {
        sideUp = "tails";
    }

}
void Coin::accumulator(string s)
{
    if (s == "heads")
    {
        headAccumulator++;
    }
    
    if (s == "tails")
    {
        tailAccumulator++;
    }
    
}

void Coin::output(int h, int t)
{
    cout << "Flipped heads: " << h << " Times" << endl;
    cout << "Flipped tails: " << t << " Times" << endl;
}