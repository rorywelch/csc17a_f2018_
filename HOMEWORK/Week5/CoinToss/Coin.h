/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Coin.h
 * Author: s
 *
 * Created on October 7, 2018, 11:03 PM
 */

#ifndef COIN_H
#define COIN_H
#include <string>
#include <cstring>
#include <ctime>
#include <iostream>
#include <cstdlib>

using namespace std;

class Coin {
public:
    Coin();
    Coin(const Coin& orig);
    virtual ~Coin();
    void toss();
    void output(int, int);
    void accumulator(string);
     string getSideUp() const
    { return sideUp;}
     int getHeadAccumulator() const
     { return headAccumulator;}
     int getTailAccumulator() const
     { return tailAccumulator;}
private:
    string sideUp;
    int headsTails;
    int headAccumulator =0;
    int tailAccumulator =0;
    const int MIN_VALUE = 1;
    const int MAX_VALUE = 2;
   

};

#endif /* COIN_H */

