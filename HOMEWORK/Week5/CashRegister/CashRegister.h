/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CashRegister.h
 * Author: s
 *
 * Created on October 10, 2018, 2:48 PM
 */

#ifndef CASHREGISTER_H
#define CASHREGISTER_H

class CashRegister {
public:
    CashRegister();
    CashRegister(const CashRegister& orig);
    virtual ~CashRegister();
    
    double getProfit() const
    {return profit;}
    double getSalestax() const
    {return salesTax;}
    double getSubTotal() const
    {return subTotal;}
    double getTotal() const
    {return total;}
    
    
     void setProfit(double);
     void setSubTotal(double, double);
     void setSalesTax();
     void setTotal();
     void outPut();
     

private:
    double profit = 0;
    double salesTax = 0;
    double subTotal = 0;
    double total = 0;

};



#endif /* CASHREGISTER_H */

