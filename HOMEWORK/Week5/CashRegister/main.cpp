/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: s
 *
 * Created on October 10, 2018, 2:47 PM
 */

#include "CashRegister.h"
#include "Inventory.h"
#include <iostream> 
#include <cstdlib>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    Inventory forRegister;
    CashRegister register1;
    
//    fixed;
    
    forRegister.setItemNumber();
    forRegister.setQuantity();     
    forRegister.setCost();
    forRegister.setOnHand();
    
    register1.setProfit(forRegister.getCost());
    register1.setSubTotal(forRegister.getCost(), forRegister.getQuantity());
    register1.setSalesTax();
    register1.setTotal();
    register1.outPut();
    
    
    

    return 0;
}

