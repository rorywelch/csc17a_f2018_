/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Inventory.h
 * Author: s
 *
 * Created on October 5, 2018, 7:07 PM
 */

#ifndef INVENTORY_H
#define INVENTORY_H

class Inventory {
public:
    Inventory();
    Inventory(const Inventory& orig);

    int getItemNumber() const
   { return itemNumber; }
    
    int getQuantity() const
   { return quantity; }
    
    double getCost() const
    {return cost;}
    
    double getTotalCost() const
   { return totalCost;} 
    
    int getOnHand() const
    {return onHand;}
    
    void setItemNumber();
    void setQuantity();
    void setCost();
    void setTotalCost();
    void setOnHand();
    
private:
    int itemNumber;
    int quantity;
    double cost;
    double totalCost;
    int onHand;
};

#endif /* INVENTORY_H */

