/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CashRegister.cpp
 * Author: s
 * 
 * Created on October 10, 2018, 2:48 PM
 */

#include "CashRegister.h"
#include "Inventory.h"
#include <iostream> 
#include <cstdlib>
#include <iomanip>
using namespace std;

CashRegister::CashRegister() {
    
   profit = 0;
   salesTax = 0;
   subTotal = 0;
   total = 0;
 }

CashRegister::CashRegister(const CashRegister& orig) {
}

CashRegister::~CashRegister() {
}



void CashRegister::setProfit(double c){
    
    profit = c * 0.3;
}

void CashRegister::setSubTotal(double c, double q){
    
    subTotal =(c + profit) * q;
}

void CashRegister::setSalesTax(){
    
    salesTax = subTotal * 0.06;
}

void CashRegister::setTotal(){
    
    total = subTotal + salesTax;
}

void CashRegister::outPut() {
    
    
    
    cout << fixed << setprecision(2) << "SubTotal: " << subTotal << endl;
    cout << fixed << setprecision(2) << "Tax:      " << salesTax << endl;
    cout << fixed << setprecision(2) << "Total:    " << total << endl;
    
    
}






