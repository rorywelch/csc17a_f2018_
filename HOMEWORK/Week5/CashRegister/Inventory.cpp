/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Inventory.cpp
 * Author: s
 * 
 * Created on October 5, 2018, 7:07 PM
 */

#include "Inventory.h"
#include <iostream> 
#include <cstdlib>
#include <iomanip>
using namespace std;


Inventory::Inventory() 
{
 itemNumber = 0;
 quantity = 0;
 cost = 0;
 totalCost=0;
 onHand = 100;
}

void Inventory::setItemNumber()
{
    do
    {
        cout << "Enter the item number: " << endl;
        cin >> itemNumber;
    }while (itemNumber < 0);
}
    
void Inventory::setQuantity()
{
    do
    {
        cout << "Enter the quantity: " << endl;
        cin >> quantity;
    }while (quantity < 0 || quantity > 100);
}
void Inventory::setCost()
{
    do
    {
        cout << "Enter the cost: " << endl;
        cin >> cost;
    }while (cost < 0);
}
void Inventory::setTotalCost()
{
    totalCost = quantity * cost;
}

void Inventory::setOnHand(){
    onHand = onHand - quantity;
}