/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: s
 *
 * Created on October 6, 2018, 7:11 PM
 */

#include "NumberArray.h"
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <cctype>

using namespace std;

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    
    
    int arrayLength1;
    
    cout << "Enter the number of slots for storing values" << endl;
    cin >> arrayLength1;
    
    NumberArray test(arrayLength1);
    
    test.storeValues(arrayLength1);
    
    test.accessValues(arrayLength1);
    
    test.output(arrayLength1);
    

    return 0;
}

