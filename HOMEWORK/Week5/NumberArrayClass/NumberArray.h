/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NumberArray.h
 * Author: s
 *
 * Created on October 6, 2018, 7:12 PM
 */

#ifndef NUMBERARRAY_H
#define NUMBERARRAY_H
#include <iostream>
#include <cstring>
#include <string>
#include <cctype>

class NumberArray {
public:
    NumberArray();
    NumberArray(int arrayLength);
    NumberArray(const NumberArray& orig);
    virtual ~NumberArray();
    void setArrayLength();
    void storeValues(int);
    void accessValues(int);
    void output(int);
    
private:

    float* numArray;
    char sentinel = 'n';
    int element;
    int inputValue;
    int accumulator = 0;
    float highest;
    float lowest;
    float average;
    

};

#endif /* NUMBERARRAY_H */

