/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DayOfYear.h
 * Author: s
 *
 * Created on October 10, 2018, 7:13 PM
 */

#ifndef DAYOFYEAR_H
#define DAYOFYEAR_H

#include <string>
#include <cstring>
#include <cstdlib>

using namespace std;

class DayOfYear {
public:
    DayOfYear();
    DayOfYear(const DayOfYear& orig);
    virtual ~DayOfYear();
    DayOfYear(int);
    void setDate();
    int getDay() const
   { return day; }
    
private:
    
    int day;
    string month;
    int monthDay;
    
};

#endif /* DAYOFYEAR_H */

