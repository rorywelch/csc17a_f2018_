/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DayOfYear.cpp
 * Author: s
 * 
 * Created on October 10, 2018, 7:13 PM
 */

#include "DayOfYear.h"
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>

using namespace std;

DayOfYear::DayOfYear(int d) {
    
    day = d;
    
}

DayOfYear::DayOfYear(const DayOfYear& orig) {
}

DayOfYear::~DayOfYear() {
}

void DayOfYear::setDate(){
    
    
    
    if (day > 0 && day <= 31)
    {
        month = "January ";
        monthDay = day;
    }
    
    if (day > 31 && day <= 59)
    {
        month = "February ";
        monthDay = day-31;
    }
    
    if (day > 59 && day <= 90)
    {
        month = "March ";
        monthDay = day-59;
    }
    
    if (day > 90 && day <= 120)
    {
        month = "April ";
        monthDay = day-90;
    }
    
    if (day > 120 && day < 151)
    {
        month = "May ";
        monthDay = day-120;
    }
    
    if (day > 151 && day <= 181)
    {
        month = "June ";
        monthDay = day-151;
    }
    
    if (day > 181 && day <= 212)
    {
        month = "July ";
        monthDay = day-181;
    }
    
    if (day > 212 && day <= 243)
    {
        month = "August ";
        monthDay = day-212;
    }
    
    if (day > 243 && day <= 273)
    {
        month = "September ";
        monthDay = day-243;
    }
    
    if (day > 273 && day <= 304)
    {
        month = "October ";
        monthDay = day-273;
    }
    
    if (day > 304 && day < 334)
    {
        month = "November ";
        monthDay = day-304;
    }
    
    if (day > 334 && day <= 365)
    {
        month = "December ";
        monthDay = day-334;
    }
    
    cout << month << monthDay << endl;
    
}