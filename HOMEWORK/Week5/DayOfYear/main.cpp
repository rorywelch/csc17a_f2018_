/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: s
 *
 * Created on October 10, 2018, 7:13 PM
 */

#include "DayOfYear.h"
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */


int main(int argc, char** argv) {
    
    int day;
       
    
    do
    {
    cout << "Enter a day between 1 and 365 and this program will" << endl;
    cout << "give you the day of the year" << endl;
    cin >> day;
    
    }while (day <= 0 || day >= 366);
    
    DayOfYear date(day);
    
   
    
    
    
    date.setDate();
        

    return 0;
}

