/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FreezingAndBoilingPoints.cpp
 * Author: s
 * 
 * Created on October 8, 2018, 8:01 PM
 */

#include "FreezingAndBoilingPoints.h"
#include <cstdlib>
#include <iostream>

using namespace std;

FreezingAndBoilingPoints::FreezingAndBoilingPoints() {
}

FreezingAndBoilingPoints::FreezingAndBoilingPoints(const FreezingAndBoilingPoints& orig) {
}

FreezingAndBoilingPoints::~FreezingAndBoilingPoints() {
}
void FreezingAndBoilingPoints::setTemperature(){
    cout << "Enter a temperature in degrees Fahrenheit" << endl;
    cin >> temperature;
    
}

void FreezingAndBoilingPoints::isEthylFreezing(float t){
    if(t <= -173)
    {
        ethylFreeze = true;
    }
    else ethylFreeze = false;
}

void FreezingAndBoilingPoints::isEthylBoiling(float t){
    if(t >= 172)
    {
        ethylBoil = true;
    }
    else ethylBoil = false;
}

void FreezingAndBoilingPoints::isOxyFreezing(float t){
    if(t <= -362)
    {
        oxyFreeze = true;
    }
    else oxyFreeze = false;
}

void FreezingAndBoilingPoints::isOxyBoiling(float t){
    if(t >= -302)
    {
        oxyBoil = true;
    }
    else oxyBoil = false;
}

void FreezingAndBoilingPoints::isWaterFreezing(float t){
     if(t <= 32)
    {
        waterFreeze = true;
    }
     else waterFreeze = false;
}

void FreezingAndBoilingPoints::isWaterBoiling(float t){
     if(t >= 212)
    {
        waterBoil = true;
    }
     else waterBoil = false;
}

void FreezingAndBoilingPoints::outPut(){
    if (ethylFreeze == 1)
    {
        cout << "Ethyl Alcohol is freezing" << endl;
    }
    if (ethylBoil == 1)
    {
        cout << "Ethyl Alcohol is boiling" << endl;
    }
    if (oxyFreeze == 1)
    {
        cout << "Oxygen is freezing" << endl;
    }
    if (oxyBoil == 1)
    {
        cout << "Oxygen is boiling" << endl;
    }
    if (waterFreeze == 1)
    {
        cout << "Water is freezing" << endl;
    }
    if (waterBoil == 1)
    {
        cout << "Water is boiling" << endl;
    }
}