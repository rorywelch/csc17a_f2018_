/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FreezingAndBoilingPoints.h
 * Author: s
 *
 * Created on October 8, 2018, 8:01 PM
 */

#ifndef FREEZINGANDBOILINGPOINTS_H
#define FREEZINGANDBOILINGPOINTS_H
#include <cstdlib>
#include <iostream>

using namespace std;

class FreezingAndBoilingPoints {
public:
    FreezingAndBoilingPoints();
    FreezingAndBoilingPoints(const FreezingAndBoilingPoints& orig);
    virtual ~FreezingAndBoilingPoints();
    void setTemperature();
    void isEthylFreezing(float);
    void isEthylBoiling(float);
    void isOxyFreezing(float);
    void isOxyBoiling(float);
    void isWaterFreezing(float);
    void isWaterBoiling(float);
    void outPut();
    float getEthylFreeze() const
     { return ethylFreeze;}
    float getEthylBoil() const
     { return ethylBoil;}
    float getOxyFreeze() const
     { return oxyFreeze;}
    float getOxyBoil() const
     { return oxyBoil;}
    float getWaterFreeze() const
     { return waterFreeze;}
    float getWaterBoil() const
     { return waterBoil;}
    float getTemperature() const
     { return temperature;}
    
private:
    float temperature;
    bool ethylFreeze;
    bool ethylBoil;
    bool oxyFreeze;
    bool oxyBoil;
    bool waterFreeze;
    bool waterBoil;
    

};

#endif /* FREEZINGANDBOILINGPOINTS_H */

