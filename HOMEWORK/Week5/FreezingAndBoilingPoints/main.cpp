/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: s
 *
 * Created on October 8, 2018, 8:00 PM
 */

#include <cstdlib>
#include <iostream>

#include "FreezingAndBoilingPoints.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    FreezingAndBoilingPoints FreezeBoil;
    
    FreezeBoil.setTemperature();
    
    FreezeBoil.isEthylFreezing(FreezeBoil.getTemperature());
    
    FreezeBoil.isEthylBoiling(FreezeBoil.getTemperature());
     
    FreezeBoil.isOxyFreezing(FreezeBoil.getTemperature());
      
    FreezeBoil.isOxyBoiling(FreezeBoil.getTemperature());
       
    FreezeBoil.isWaterFreezing(FreezeBoil.getTemperature());
        
    FreezeBoil.isWaterBoiling(FreezeBoil.getTemperature());
    
    FreezeBoil.outPut();
   

    return 0;
}

