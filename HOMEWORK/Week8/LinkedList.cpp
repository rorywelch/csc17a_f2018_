/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LinkedList.cpp
 * Author: s
 * 
 * Created on November 17, 2018, 4:18 PM
 */

#include "LinkedList.h"

LinkedList::LinkedList() {
head = NULL;
  
}

LinkedList::LinkedList(const LinkedList& orig) {
}

LinkedList::~LinkedList() {
    Node* nodePtr = head;
    while(nodePtr)
    {
        Node* temp = nodePtr;
        nodePtr = nodePtr->next;
        
        delete temp;
    }
}

void LinkedList::append(int val){
    Node* newNode;
    Node* nodePtr;
    
    newNode = new Node;
    
    newNode->value = val;
    newNode->next = nullptr;
    
    if(!head)
    {
        head =newNode;
    }
    else
    {
        
        nodePtr = head;
        
        while(nodePtr->next)
        {
            nodePtr = nodePtr->next;
        }
            nodePtr->next = newNode;
        
    
    }
}

void LinkedList::insert(int val){
    Node* newNode;
    Node* nodePtr;
    Node* prev = nullptr;
    
    newNode = new Node;
    newNode->value = val;
    
    if(!head)
    {
        head = newNode;
        newNode->next = nullptr;
    }
    else
    {
        nodePtr = head;
        prev = nullptr;
    
    
    while(nodePtr != nullptr && nodePtr->value < val)
    {
        prev = nodePtr;
        nodePtr = nodePtr->next;
    }
    if(prev == nullptr)
    {
        head = newNode;
        newNode->next = nodePtr;
    }
    else
    {
        prev->next = newNode;
        newNode->next = nodePtr;
    }
    }
    }
    
void LinkedList::print() const{
    Node* nodePtr = head;
    while(nodePtr)
    {
        cout << nodePtr->value << " " << endl;
        nodePtr = nodePtr->next;
    }
}

void LinkedList::remove(int val){
    
    Node* nodePtr;
    Node* prev;
    
    if(!head)
    {
        nodePtr = head->next;
        delete head;
        head = nodePtr;
    }
    else
    {
        nodePtr = head;
        
        while(nodePtr != nullptr && nodePtr->value != val)
        {
            prev = nodePtr;
            nodePtr = nodePtr->next;
        }
        
        if(nodePtr)
        {
            prev->next = nodePtr->next;
            delete nodePtr;
        }
    }
}
