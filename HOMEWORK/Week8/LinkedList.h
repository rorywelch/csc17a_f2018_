/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LinkedList.h
 * Author: s
 *
 * Created on November 17, 2018, 4:18 PM
 */

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <cstdlib>
#include <iostream>

using namespace std;

class LinkedList {
public:



    LinkedList();
    LinkedList(const LinkedList& orig);
    virtual ~LinkedList();
    void append(int);
    void insert(int);
    void remove(int);
    void print()const;
    

private:
struct Node
{
    int value;
    Node* next;
};
    Node* head;

};

#endif /* LINKEDLIST_H */

