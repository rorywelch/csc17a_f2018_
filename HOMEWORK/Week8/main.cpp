/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: s
 *
 * Created on November 17, 2018, 4:18 PM
 */

#include <cstdlib>
#include "LinkedList.h"
using namespace std;

/*
 * 
 */


int main(int argc, char** argv) {
    
    LinkedList Test;
    
    Test.append(1);
    Test.append(2);

    Test.append(4);
    Test.append(5);
    
    Test.insert(3);
    
    Test.print();
    
    cout << endl;
    
    Test.remove(3);
            
    Test.print();        
    
    return 0;
}

