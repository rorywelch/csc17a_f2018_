#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    const int SIZE = 5;    
    char codeChar[SIZE];
    
    fstream file;

//// Open the file for input in binary mode.
 file.open("test.dat", ios::in | ios::binary);

 // Read the contents of the file into the array.
 cout << "Now reading the data back into memory.\n";
 file.read(codeChar, sizeof(codeChar));
 
         for(int i = 0; i < SIZE; i++)
    {
        codeChar[i] = codeChar[i] - 10;
    }

 // Display the contents of the array.
 for (int count = 0; count < SIZE; count++)
 cout << codeChar[count] << " ";
 cout << endl;

 // Close the file.
 file.close();
 return 0;
 } 