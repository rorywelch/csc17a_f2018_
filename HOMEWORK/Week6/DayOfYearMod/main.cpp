/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: s
 *
 * Created on October 10, 2018, 7:13 PM
 */

#include "DayOfYear.h"
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */


int main(int argc, char** argv) {
    
    int day;
    string monthString;
       
    
    
    cout << "Enter a day between 1 and 31 then enter a month" << endl;
    cout << "this program will display the date and increment and" << endl;
    cout << "decrement the day" << endl;
    
    cout << "Enter a numerical day" << endl;
    cin >> day;
    cout << "Enter a month non-numerically" << endl;
    cin >> monthString;
    
    
    
    DayOfYear date(monthString, day);
    
 
    date.setDate();
    
    ++date;
    
    date.setDate();
    
    date++;
       
    date.setDate();
    
    --date;
    
    date.setDate();
    
    date--;
    
    date.setDate();
    
    --date;
    
    date.setDate();
    
    date--;
    
    date.setDate();
    
    ++date;
    
    date.setDate();
    
    date++;
    
    date.setDate();

    return 0;
}

