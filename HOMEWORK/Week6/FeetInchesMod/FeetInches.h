/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeetInches.h
 * Author: s
 *
 * Created on October 31, 2018, 2:22 PM
 */

 #ifndef FEETINCHES_H
 #define FEETINCHES_H

 // The FeetInches class holds distances or measurements
 // expressed in feet and inches.

 class FeetInches
 {
 private:
 int feet; // To hold a number of feet
 int inches; // To hold a number of inches
 bool status;
 void simplify(); // Defined in FeetInches.cpp
 public:
 // Constructor
 FeetInches(int f = 0, int i = 0)
 { feet = f;
 inches = i;
 simplify(); }
 
 FeetInches(const FeetInches&); 

 // Mutator functions
void setFeet(int f)
 { feet = f; } 
void setInches(int i)
 { inches = i;
 simplify(); }

 // Accessor functions
 int getFeet() const
 { return feet; }

 int getInches() const
 { return inches; }

 // Overloaded operator functions
 FeetInches operator + (const FeetInches &); // Overloaded +
 FeetInches operator - (const FeetInches &); // Overloaded −
 FeetInches operator * (const FeetInches &);
 bool operator <= (const FeetInches &);
 bool operator >= (const FeetInches &);
 bool operator != (const FeetInches &);
 };

 #endif 