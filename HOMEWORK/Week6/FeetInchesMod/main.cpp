 // This program demonstrates the FeetInches class's overloaded
 // relational operators.
 #include <iostream>
 #include "FeetInches.h"
 using namespace std;

 int main()
 {
 int feet, inches; // To hold input for feet and inches

 // Create two FeetInches objects. The default arguments
 // for the constructor will be used.
 FeetInches first;

 // Get a distance from the user.
 cout << "Enter a distance in feet and inches: ";
 cin >> feet >> inches;

 // Store the distance in first.
 first.setFeet(feet);
 first.setInches(inches);

 // Copy Constructor
 FeetInches second(first);
 
 cout << "Copy constructor feet" << endl;
 cout << second.getFeet() << endl;
 cout << "Copy constructor inches" << endl;
 cout << second.getInches() << endl;
 

  first = first * second;
 
cout << "Multiply operator for original input times cop constructor" << endl;
cout << "Multiply operator feet" << endl;
cout << first.getFeet() << endl;
cout << "Multiply operator inches" << endl;
cout << first.getInches() << endl;

 
 
return 0;
} 