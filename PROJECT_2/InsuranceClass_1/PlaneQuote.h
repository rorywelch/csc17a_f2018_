

#ifndef PLANEQUOTE_H
#define PLANEQUOTE_H

#include "InsuranceQuote.h"
using namespace std;

class PlaneQuote :public InsuranceQuote {
public:
    PlaneQuote();
    PlaneQuote(const PlaneQuote& orig);
    virtual ~PlaneQuote();
    
    //Only use modified functions
    void setPolicies(); 
    void setTotal();
    void output();
   
};

#endif /* BOATQUOTE_H */

