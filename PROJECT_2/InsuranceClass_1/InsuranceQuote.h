

#ifndef INSURANCEQUOTE_H
#define INSURANCEQUOTE_H
#include <iostream> 
#include <cstdlib>
#include <cctype>
#include <iomanip>
#include <string>
using namespace std;

class InsuranceQuote {

    
public:
    int coverageLength;       //size of coverage array
    char company;              //matches list of selectable companies
    int* coverages;           //dynamic array holding coverages
    char policyChoice;         //input to choose a policy
    int age;                  //input for age
    int collisionNum;         //number of crashes on driving history
    float costAccum;          //accumulates the cost of coverages
    float costAdd;            //used to hold cost added to coverage cost
    float total;              //used for output 
    char policySentinel;      //sentinel value to determine if more policies are wanted
    string nameF;             //first name input
    string nameL;             //last name input
    
    InsuranceQuote();        //default constructor
    InsuranceQuote(const InsuranceQuote& orig); //copy constructor
    virtual ~InsuranceQuote(); //destructor
    
    void setCompany();            //setters using IO when called
    void setCompany(char);
    void setPolicy(char, int);
    void setPolicies();
    void setPersonal();
    void setTotal();
    void output();                //used to display insurance quote
    
                                  //getter prototypes
    int getCompany(){return company;};
    int getCoverages(const InsuranceQuote& orig, int ,int);
    int getPolicyChoice(){return policyChoice;};
    int getCollisionNum(){return collisionNum;}
    float getCostAccum(){return costAccum;}
    float getCostAdd() {return costAdd;}
    float getTotal(){return total;}
    char getPolicySentinel(){return policySentinel;}
    string getNameF(){return nameF;}
    string getNameL() {return nameL;}
    
    //Exception Handling classes
    class badCompanyChoice
    {};
    class badPolicyChoice
    {};
 
};

#endif /* INSURANCEQUOTE_H */

