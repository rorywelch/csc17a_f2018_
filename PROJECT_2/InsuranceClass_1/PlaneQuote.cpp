

#include "PlaneQuote.h"
#include "InsuranceQuote.h"



PlaneQuote::PlaneQuote():InsuranceQuote() {
    
}

PlaneQuote::PlaneQuote(const PlaneQuote& orig):InsuranceQuote() {
    
}

PlaneQuote::~PlaneQuote() {
}

void PlaneQuote::setPolicies(){
    
//Prompt
    cout << "Enter which policies you want one at a time" << endl;
    
    
    if (company == 1)
{
        coverageLength = 9;
        
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    if (company == 2)
{        
        coverageLength = 6;
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    if (company == 3)
{
        coverageLength = 4;           
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    
    
    
   do{
       
    bool tryagain = true;
    while(tryagain){
    try
    {
       if (company == 1)
{
        cout << "Liability = 1" << endl;
        cout << "Collision = 2" << endl;
        cout << "Emergency landing assistance = 3" << endl;
        cout << "Comprehensive = 4" << endl;
        cout << "Uninsured pilot = 5" << endl;
        cout << "Underinsured pilot = 6" << endl;
        cout << "Rental Plane = 7" << endl; 
        cout << "Storm damage = 8" << endl;
        cout << "Terrorist Assistance = 9" << endl; 
       }
       
       if (company == 2)
{
        cout << "Liability = 1" << endl;
        cout << "Collision = 2" << endl;
        cout << "Emergency landing assistance = 3" << endl;
        cout << "Comprehensive = 4" << endl;
        cout << "Rental plane = 5" << endl; 
        cout << "Storm damage = 6" << endl;
        cout << "Terrorist Assistance = 7" << endl;
       }
       
       if (company == 3)
{
        cout << "Liability = 1" << endl;
        cout << "Collision = 2" << endl;
        cout << "Emergency landing assistance = 3" << endl;
        cout << "Comprehensive = 4" << endl;
        cout << "Terrorist Assistance = 5" << endl;
       }
       
   
       cin >> policyChoice;
       setPolicy(policyChoice, coverageLength);
       tryagain = false;
    }       
    catch(InsuranceQuote::badPolicyChoice)
    {
        cout << "Invalid Policy Choice" << endl;
    }
    }
       
             
       coverages[policyChoice-1] = 1;
       
       
       cout << "Do you want to choose another policy?" << endl;
       cout << "Enter Y/N" << endl;
       cin >> policySentinel;
       
       while(!toupper(policySentinel) == 'Y' || !toupper(policySentinel) == 'N') {
           cout << "Invalid Choice" << endl;
           cout << "Enter Y/N" << endl;
           cin >> policySentinel;
       }
    
            
       
}while (toupper(policySentinel) == 'Y');

}

void PlaneQuote::setTotal(){
     costAccum = 0;
     
         if (company == 1){
         if (coverages[0]==1)
             costAccum+=50.0;
         if (coverages[1]==1)
             costAccum+=50.0;
         if (coverages[2]==1)
             costAccum+=20.0;
         if (coverages[3]==1)
             costAccum+=15.0;
         if (coverages[4]==1)
             costAccum+=10.0;
         if (coverages[5]==1)
             costAccum+=10.0;
         if (coverages[6]==1)
             costAccum+=10.0;
         if (coverages[7]==1)
             costAccum+=20.0;
         if (coverages[8]==1)
             costAccum+=25.0;
         if (age >= 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum)); 
      }
     
      if (company == 2){
         if (coverages[0]==1)
             costAccum+=45.0;
         if (coverages[1]==1)
             costAccum+=45.0;
         if (coverages[2]==1)
             costAccum+=15.0;
         if (coverages[3]==1)
             costAccum+=15.0;
         if (coverages[4]==1)
             costAccum+=8.0;
         if (coverages[5]==1)
             costAccum+=15.0;
         if (coverages[6]==1)
             costAccum+=15.0;
         if (age >= 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum));
     
 }
 
     if (company == 3){
         if (coverages[0]==1)
             costAccum+=25.0;
         if (coverages[1]==1)
             costAccum+=25.0;
         if (coverages[2]==1)
             costAccum+=5.0;
         if (coverages[3]==1)
             costAccum+=3.0;
         if (coverages[4]==1)
             costAccum+=5.0;
         if (age >= 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum)); 
     
 }
     
      
 }

void PlaneQuote::output(){
     
     total = costAdd;
     
    
     cout << nameF << " " << nameL << endl;
     
     
     if(company == 1){
         cout << "Company: Smercury" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Liability: $50.00" << endl;
         if(coverages[1] == 1)
             cout << "Collision: $50.00" << endl;
         if(coverages[2] == 1)
             cout << "Emergency landing assistance: $20.00" << endl;
         if(coverages[3] == 1)
             cout << "Comprehensive: $15.00" << endl;
         if(coverages[4] == 1)
             cout << "Uninsured pilot: $10.00" << endl;
         if(coverages[5] == 1)
             cout << "Underinsured pilot: $10.00" << endl;
         if(coverages[6] == 1)
             cout << "Rental plane: $10.00" << endl;
         if(coverages[7] == 1)
             cout << "Storm damage: $20.00" << endl;
         if(coverages[8] == 1)
             cout << "Terrprist Assistance: $25.00" << endl;
         
         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for age and driving history" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
         
                  
     }
     
          
     
     if(company == 2){
         cout << "Company: Some State" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Liability: $45.00" << endl;
         if(coverages[1] == 1)
             cout << "Collision: $45.00" << endl;
         if(coverages[2] == 1)
             cout << "Emergency landing assistance: $15.00" << endl;
         if(coverages[3] == 1)
             cout << "Comprehensive: $15.00" << endl;
         if(coverages[4] == 1)
             cout << "Rental plane: $8.00" << endl;
         if(coverages[5] == 1)
             cout << "Storm damage: $15.00" << endl;
         if(coverages[6] == 1)
             cout << "Terrorist assistance: $15.00" << endl;
         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for age and driving history" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
     }
     
           
     
     if(company == 3){
         cout << "Company: Ghetto Insurance" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Liability: $25.00" << endl;
         if(coverages[1] == 1)
             cout << "Collision: $25.00" << endl;
         if(coverages[2] == 1)
             cout << "Emergency landing assistance: $5.00" << endl;
         if(coverages[3] == 1)
             cout << "Comprehensive: $3.00" << endl;
         if(coverages[4] == 1)
             cout << "Terrorist assistance: $5.00" << endl;

         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for age and driving history" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
     }
     
     
 }