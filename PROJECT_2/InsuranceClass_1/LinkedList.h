

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "InsuranceQuote.h"
#include <iostream> 
#include <cstdlib>
#include <cctype>
#include <iomanip>
#include <string>
using namespace std;


template <class T> 
 class ListNode
 {
 public:
 T value; // Node value
 ListNode<T> *next; // Pointer to the next node
  
 // Constructor
 
 ListNode (T nodeValue)
 { value = nodeValue;
 next = nullptr; }
 }; 
 
template <class T>
 class LinkedList
 {
 private:
 ListNode<T> *head; // List head pointer
 
 public:
 // Constructor
 LinkedList()
 { head = NULL; }
 
 // Destructor
 ~LinkedList();
  // Linked list operations
 void appendNode(T);
 void displayList() const;
 }; 
    
 template <class T>
 void LinkedList<T>::appendNode(T newValue)
 {
 ListNode<T> *newNode; // To point to a new node
 ListNode<T> *nodePtr; // To move through the list

 // Allocate a new node and store newValue there.
 newNode = new ListNode<T>(newValue);

 // If there are no nodes in the list
 // make newNode the first node. 

 if (!head)
 head = newNode;
 else // Otherwise, insert newNode at end.
 {
 // Initialize nodePtr to head of list.
 nodePtr = head;
  // Find the last node in the list.
 while (nodePtr->next)
 nodePtr = nodePtr->next;
  // Insert newNode as the last node.
 nodePtr->next = newNode;
 }
 } 
 
 
 
template <class T>
void LinkedList<T>::displayList() const
{
ListNode<T> *nodePtr; // To move through the list

// Position nodePtr at the head of the list.
nodePtr = head;

// While nodePtr points to a node, traverse
// the list.
while (nodePtr)
{
// Display the value in this node.
nodePtr->value.output();

// Move to the next node.
nodePtr = nodePtr->next;
}
} 
 


template <class T>
LinkedList<T>::~LinkedList()
{ 
 
ListNode<T> *nodePtr; // To traverse the list
ListNode<T> *nextNode; // To point to the next node
// Position nodePtr at the head of the list.
nodePtr = head;

// While nodePtr is not at the end of the list...
while (nodePtr != NULL)
{
// Save a pointer to the next node.
nextNode = nodePtr->next; 

// Delete the current node.
delete nodePtr;
// Position nodePtr at the next node.
nodePtr = nextNode;
}
} 


#endif /* LINKEDLIST_H */
