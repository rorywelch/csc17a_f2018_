

#include "PropertyQuote.h"
#include "InsuranceQuote.h"



PropertyQuote::PropertyQuote():InsuranceQuote() {
    
}

PropertyQuote::PropertyQuote(const PropertyQuote& orig):InsuranceQuote() {
    
}

PropertyQuote::~PropertyQuote() {
}

void PropertyQuote::setPolicies(){
    
//Prompt
    cout << "Enter which policies you want one at a time" << endl;
    
    
    if (company == 1)
{
        coverageLength = 8;
        
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    if (company == 2)
{        
        coverageLength = 6;
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    if (company == 3)
{
        coverageLength = coverageLength;           
        coverages = new int [coverageLength];
    
    for (int i = 0; i < 4 ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    
    
    
   do{
       
    bool tryagain = true;
    while(tryagain){
    try
    {
       if (company == 1)
{
        cout << "Renter's = 1" << endl;
        cout << "Home owner's = 2" << endl;
        cout << "Weather damage = 3" << endl;
        cout << "Flood = 4" << endl;
        cout << "Fire = 5" << endl;
        cout << "Burglary = 6" << endl;
        cout << "Squatter removal = 7" << endl; 
        cout << "Commercial property = 8" << endl;
         
       }
       
       if (company == 2)
{
        cout << "Renter's = 1" << endl;
        cout << "Home owner's = 2" << endl;
        cout << "Weather damage = 3" << endl;
        cout << "Flood = 4" << endl;
        cout << "Fire = 5" << endl;
        cout << "Commercial property = 6" << endl;
       }
       
       if (company == 3)
{
        cout << "Renter's = 1" << endl;
        cout << "Home owner's = 2" << endl;
        cout << "Burglary = 3" << endl;
        cout << "Squatter removal = 4" << endl; 
       }
       
      
       cin >> policyChoice;
       setPolicy(policyChoice, coverageLength);
       tryagain = false;
    }       
    catch(InsuranceQuote::badPolicyChoice)
    {
        cout << "Invalid Policy Choice" << endl;
    }
    }
       
       if (company == 1){
       while (policyChoice < 1 ||
              policyChoice > 8){
       
           cout << "Invalid Choice" << endl;
           cin >> policyChoice;
       }
    }
       
       if (company == 2){
       while (policyChoice < 1 ||
              policyChoice > 6){
       
           cout << "Invalid Choice" << endl;
           cin >> policyChoice;
       }
    }
    
    if (company == 3){
       while (policyChoice < 1 ||
              policyChoice > 4){
       
           cout << "Invalid Choice" << endl;
           cin >> policyChoice;
       }
    }
       
       coverages[policyChoice-1] = 1;
       
       
       cout << "Do you want to choose another policy?" << endl;
       cout << "Enter Y/N" << endl;
       cin >> policySentinel;
       
       while(!toupper(policySentinel) == 'Y' || !toupper(policySentinel) == 'N') {
           cout << "Invalid Choice" << endl;
           cout << "Enter Y/N" << endl;
           cin >> policySentinel;
       }
    
            
       
}while (toupper(policySentinel) == 'Y');

}

void PropertyQuote::setTotal(){
     costAccum = 0;
     
         if (company == 1){
         if (coverages[0]==1)
             costAccum+=50.0;
         if (coverages[1]==1)
             costAccum+=50.0;
         if (coverages[2]==1)
             costAccum+=20.0;
         if (coverages[3]==1)
             costAccum+=15.0;
         if (coverages[4]==1)
             costAccum+=10.0;
         if (coverages[5]==1)
             costAccum+=10.0;
         if (coverages[6]==1)
             costAccum+=10.0;
         if (coverages[7]==1)
             costAccum+=20.0;
         if (age >= 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum)); 
      }
     
      if (company == 2){
         if (coverages[0]==1)
             costAccum+=45.0;
         if (coverages[1]==1)
             costAccum+=45.0;
         if (coverages[2]==1)
             costAccum+=15.0;
         if (coverages[3]==1)
             costAccum+=15.0;
         if (coverages[4]==1)
             costAccum+=8.0;
         if (coverages[5]==1)
             costAccum+=15.0;
         if (age > 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum));
     
 }
 
     if (company == 3){
         if (coverages[0]==1)
             costAccum+=25.0;
         if (coverages[1]==1)
             costAccum+=25.0;
         if (coverages[2]==1)
             costAccum+=5.0;
         if (coverages[3]==1)
             costAccum+=3.0;
         if (age > 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum)); 
     
 }
     
      
 }

void PropertyQuote::output(){
     
     total = costAdd;
     
    
     cout << nameF << " " << nameL << endl;
     
     
     if(company == 1){
         cout << "Company: Smercury" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Renter's: $50.00" << endl;
         if(coverages[1] == 1)
             cout << "Home owner's: $50.00" << endl;
         if(coverages[2] == 1)
             cout << "Weather damage: $20.00" << endl;
         if(coverages[3] == 1)
             cout << "Flood: $15.00" << endl;
         if(coverages[4] == 1)
             cout << "Fire: $10.00" << endl;
         if(coverages[5] == 1)
             cout << "Burglary: $10.00" << endl;
         if(coverages[6] == 1)
             cout << "Squatter removal: $10.00" << endl;
         if(coverages[7] == 1)
             cout << "Commercial propery: $20.00" << endl;
                  
         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for age and driving history" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
         
     }
     

     if(company == 2){
         cout << "Company: Some State" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Renter's: $45.00" << endl;
         if(coverages[1] == 1)
             cout << "Home owner's: $45.00" << endl;
         if(coverages[2] == 1)
             cout << "Weather damage: $15.00" << endl;
         if(coverages[3] == 1)
             cout << "Flood: $15.00" << endl;
         if(coverages[4] == 1)
             cout << "Fire: $8.00" << endl;
         if(coverages[5] == 1)
             cout << "Commercial property: $15.00" << endl;
         
         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for age and driving history" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
     }
     
           
     if(company == 3){
        cout << "Company: Ghetto Insurance" << endl;
        cout << "Coverages: " << endl;        
        if(coverages[0] == 1)
             cout << "Renter's: $25.00" << endl;
         if(coverages[1] == 1)
             cout << "Home owner's: $25.00" << endl;
         if(coverages[2] == 1)
             cout << "Burglary: $5.00" << endl;
         if(coverages[3] == 1)
             cout << "Squatter removal: $3.00" << endl;
        
         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for age and driving history" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
     }
     
     
 }


 void PropertyQuote::setPersonal(){
     
     cout << "Enter your name first name" << endl;
     cin >> nameF;
     
     cout << "Enter your last name" << endl;
     cin >> nameL;
     
     
     cout << "Enter your age" << endl;
     cin >> age;
     
     while(age < 0){
         cout << "Invalid input" << endl;
         cout << "Enter your age" << endl;
         cin >> age;
     }
     
          
     while(collisionNum < 0){
         cout << "Invalid input" << endl;
         cout << "Enter your number of at fault claims in the last three years" << endl;
         cin >> collisionNum;
     }
     
 }