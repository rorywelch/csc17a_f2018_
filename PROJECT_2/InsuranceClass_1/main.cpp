

#include "InsuranceQuote.h"  //Include all header files
#include "BoatQuote.h"
#include "PlaneQuote.h"
#include "PropertyQuote.h"
#include "LinkedList.h"
#include <iostream> 
#include <cstdlib>
#include <cctype>
#include <iomanip>
#include <string>


using namespace std;


int main(int argc, char** argv) {
    
    char sentinel;                   //used to determine if another quote is
    char insuranceType;              //to choose different insurance types
    int insuranceType2;              //used for conversion after input validation
    LinkedList<InsuranceQuote> List; //declare linked list for insurance quotes
    


    
   do{ 
       //Prompt
       cout << "Enter the type of insurance you desire" << endl;
       cout << "Enter:  1: commercial vehicle" << endl;
       cout << "Enter:  2: boat" << endl;
       cout << "Enter:  3: airplane" << endl;
       cout << "Enter:  4: property" << endl;
       
       cin >> insuranceType;
       
       insuranceType2 = insuranceType - 48;
                   
       
       while(insuranceType2 != 1 && insuranceType2 != 2 && insuranceType2 != 3&&
             insuranceType2 != 4 || !isdigit(insuranceType)){
       cout << "Invalid input" << endl;
       cout << "Enter the type of insurance you desire" << endl;
       cout << "Enter:  1: commercial vehicle" << endl;
       cout << "Enter:  2: boat" << endl;
       cout << "Enter:  3: airplane" << endl;
       cout << "Enter:  4: property" << endl;
       
       cin >> insuranceType;
       
       insuranceType2 = insuranceType - 48;
           
       }
       
       
       
       
      
       if (insuranceType2 == 1){
    
    InsuranceQuote Car;    //Declare class
    Car.setCompany();      //Call setter functions
    Car.setPolicies();
    Car.setPersonal();
    Car.setTotal();
    Car.output();
    List.appendNode(Car);  //Append to linked list
    
       }
      
    if (insuranceType2 == 2){
           
    BoatQuote Boat;         //Declare class
    Boat.setCompany();      //Call setter functions
    Boat.setPolicies();
    Boat.setPersonal();
    Boat.setTotal();
    Boat.output();
    List.appendNode(Boat);  //Append to linked list 
      }
       
       if (insuranceType2 == 3){
           
    PlaneQuote Plane;        //Declare class
    Plane.setCompany();      //Call setter functions
    Plane.setPolicies();
    Plane.setPersonal();
    Plane.setTotal();
    Plane.output();
    List.appendNode(Plane);  //Append to linked list
      }
       
       if (insuranceType2 == 4){
           
    PropertyQuote Property;     //Declare class
    Property.setCompany();      //Call setter functions
    Property.setPolicies();
    Property.setPersonal();
    Property.setTotal();
    Property.output();
    List.appendNode(Property);  //Append to linked list
      }
    
       //Allow for additional quotes    
       cout << "Do you want another quote?" << endl;
       cout << "Enter Y/N" << endl;
       cin >> sentinel;
       
       //Use while loop validation
    while(!toupper(sentinel) == 'Y' || !toupper(sentinel) == 'N') {
           cout << "Invalid Choice" << endl;
           cout << "Enter Y/N" << endl;
           cin >> sentinel;
       }
        
       //Sentinel controled do while loop   
   }while(toupper(sentinel) == 'Y');
   
   
    //Display all created quotes using linked list function
    List.displayList();
     
    return 0;
}
