

#ifndef BOATQUOTE_H
#define BOATQUOTE_H

#include "InsuranceQuote.h"
using namespace std;

class BoatQuote :public InsuranceQuote {
public:
    BoatQuote();
    BoatQuote(const BoatQuote& orig);
    virtual ~BoatQuote();
    
    //Only use modified functions
    void setPolicies(); 
    void setTotal();
    void output();
   
};

#endif /* BOATQUOTE_H */

