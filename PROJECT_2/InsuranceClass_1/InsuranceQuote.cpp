

#include "InsuranceQuote.h"
#include <iostream> 
#include <cstdlib>
#include <cctype>
#include <iomanip>
#include <string>
using namespace std;

//Default constructor
InsuranceQuote::InsuranceQuote() {
 
    // Initialize variables
    coverageLength = 0;
    company = 0;
    coverages = NULL;
    policyChoice = 0;
    age = 0;
    collisionNum = 0;
    costAccum = 0;
    costAdd = 0;
    total = 0;
    policySentinel = 'Y';
    nameF = " ";
    nameL = " ";
}


//Copy constructor
InsuranceQuote::InsuranceQuote(const InsuranceQuote& orig) {
    
    coverageLength = orig.coverageLength;
    company = orig.company;
    policyChoice = orig.policyChoice;
    age = orig.age;
    collisionNum = orig.collisionNum;
    costAccum = orig.costAccum;
    costAdd = orig.costAdd;
    total = orig.total;
    policySentinel = orig.policySentinel;
    nameF = orig.nameF;
    nameL = orig.nameL;
    
    if(company == 1)
    {
        coverageLength = 7;
        coverages = new int [coverageLength];
                for (int i = 0; i > coverageLength; i++)
                {
                    coverages[i]= orig.coverages[i];
                }
    }
    
    if(company == 2)
    {
        coverageLength = 5;
        coverages = new int [coverageLength];
                for (int i = 0; i > coverageLength; i++)
                {
                    coverages[i]= orig.coverages[i];
                }
    }
    
    if(company == 3)
    {
        coverageLength = 4;
        coverages = new int [coverageLength];
                for (int i = 0; i > coverageLength; i++)
                {
                    coverages[i]= orig.coverages[i];
                }
    }
    
    
    
}
 
//Destructor
InsuranceQuote::~InsuranceQuote() {
         delete [] coverages;
}

//Set company
void InsuranceQuote::setCompany(char i){
    //49 = 1 when converting between ints and chars
    if (i > 51 || i < 49){
       throw badCompanyChoice();
    }
    else if(!isdigit(i))
    { 
        throw badCompanyChoice(); 
    }
    else if(isdigit(i)){
    {
        company = i - 48;}
    }
    
}
void InsuranceQuote::setCompany()
{
    
    bool tryagain = true;
    
    while(tryagain){
    try
    {
       //Prompt
    cout << "Choose a company to get a quote from by entering that " << endl;
    cout << "Company's corresponding number." << endl;
    cout << "1 Smercury" << endl;
    cout << "2 Some State" << endl;
    cout << "3 Ghetto insurance" << endl;
       cin >> company;
       setCompany(company);
       tryagain = false;
           }
    catch(InsuranceQuote::badCompanyChoice)
    {
        cout << "Invalid Company Choice" << endl;
    }
    }
}

//Set policies
void InsuranceQuote::setPolicy(char i, int p){
    //49 = 1 when converting between ints and chars
    if (i > p+48 || i < 49){
       throw badPolicyChoice();
    }
    else if(!isdigit(i))
    { 
        throw badPolicyChoice(); 
    }
    else if(isdigit(i)){
    {
        policyChoice = i - 48;}
    }
}
void InsuranceQuote::setPolicies(){
    
    
    //Prompt
    cout << "Enter which policies you want one at a time" << endl;
    
    //Create dynamic data to hold policies
    if (company == 1)
{
        coverageLength = 7;
        
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    if (company == 2)
{        
        coverageLength = 5;
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    if (company == 3)
{
        coverageLength = 4;           
        coverages = new int [coverageLength];
    
    for (int i = 0; i < coverageLength ; i++)
    {
        coverages[i] = 0;
    }
    }
    
    
    
    //Loop to load in policy options
   do{
        
    bool tryagain = true;
    while(tryagain){
    try
    {
       if (company == 1)
{
        cout << "Liability = 1" << endl;
        cout << "Collision = 2" << endl;
        cout << "Roadside assistance = 3" << endl;
        cout << "Comprehensive = 4" << endl;
        cout << "Uninsured motorist = 5" << endl;
        cout << "Underinsured motorist = 6" << endl;
        cout << "Rental car = 7" << endl; 
       }
       
       if (company == 2)
{
        cout << "Liability = 1" << endl;
        cout << "Collision = 2" << endl;
        cout << "Roadside assistance = 3" << endl;
        cout << "Comprehensive = 4" << endl;
        cout << "Rental car = 5" << endl; 
       }
       
       if (company == 3)
{
        cout << "Liability = 1" << endl;
        cout << "Collision = 2" << endl;
        cout << "Roadside assistance = 3" << endl;
        cout << "Comprehensive = 4" << endl;
       }
       
       
       
    //Input validation
       cin >> policyChoice;
       setPolicy(policyChoice, coverageLength);
       tryagain = false;
    }       
    catch(InsuranceQuote::badPolicyChoice)
    {
        cout << "Invalid Policy Choice" << endl;
    }
    }
       if (company == 1){
       while (policyChoice < 1 ||
              policyChoice > 7){
       
           cout << "Invalid Choice" << endl;
           cin >> policyChoice;
       }
    }
       
       if (company == 2){
       while (policyChoice < 1 ||
              policyChoice > 5){
       
           cout << "Invalid Choice" << endl;
           cin >> policyChoice;
       }
    }
    
    if (company == 3){
       while (policyChoice < 1 ||
              policyChoice > 4){
       
           cout << "Invalid Choice" << endl;
           cin >> policyChoice;
       }
    }
       
       //Set which policies are chosen
       coverages[policyChoice-1] = 1;
       
       //Sentinel control for loop
       cout << "Do you want to choose another policy?" << endl;
       cout << "Enter Y/N" << endl;
       cin >> policySentinel;
       
       //sentinel input validation
       while(!toupper(policySentinel) == 'Y' || !toupper(policySentinel) == 'N') {
           cout << "Invalid Choice" << endl;
           cout << "Enter Y/N" << endl;
           cin >> policySentinel;
       }
    
            
       
}while (toupper(policySentinel) == 'Y');
   }
   


//input personal information which may impact calculated total
 void InsuranceQuote::setPersonal(){
     
     cout << "Enter your name first name" << endl;
     cin >> nameF;
     
     cout << "Enter your last name" << endl;
     cin >> nameL;
     
     
     cout << "Enter your age" << endl;
     cin >> age;
     
     while(age < 0){
         cout << "Invalid input" << endl;
         cout << "Enter your age" << endl;
         cin >> age;
     }
     
     cout << "Enter your number of at fault claims in the last three years" << endl;
     cin >> collisionNum;
     
     while(collisionNum < 0){
         cout << "Invalid input" << endl;
         cout << "Enter your number of at fault claims in the last three years" << endl;
         cin >> collisionNum;
     }
     
 }
 
 //Total calculated based on policy choice and personal information
 void InsuranceQuote::setTotal(){
     costAccum = 0; //Accumulator
     
     //Block holding policy costs to determine if needed to load into accumulator
         if (company == 1){
         if (coverages[0]==1)
             costAccum+=50.0;
         if (coverages[1]==1)
             costAccum+=50.0;
         if (coverages[2]==1)
             costAccum+=20.0;
         if (coverages[3]==1)
             costAccum+=15.0;
         if (coverages[4]==1)
             costAccum+=10.0;
         if (coverages[5]==1)
             costAccum+=10.0;
         if (coverages[6]==1)
             costAccum+=10.0;
         if (age >= 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum)); 
      }
     
      if (company == 2){
         if (coverages[0]==1)
             costAccum+=45.0;
         if (coverages[1]==1)
             costAccum+=45.0;
         if (coverages[2]==1)
             costAccum+=15.0;
         if (coverages[3]==1)
             costAccum+=15.0;
         if (coverages[4]==1)
             costAccum+=8.0;
         if (age >= 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum));
     
 }
 
     if (company == 3){
         if (coverages[0]==1)
             costAccum+=25.0;
         if (coverages[1]==1)
             costAccum+=25.0;
         if (coverages[2]==1)
             costAccum+=5.0;
         if (coverages[3]==1)
             costAccum+=3.0;
         if (age >= 25 && age < 55)
            costAdd = costAccum; 
         if (age < 25)
             costAdd = costAccum *2;
         if (age >= 55)
             costAdd = costAccum / 2;
         if (collisionNum > 0)
             costAdd = costAdd + (costAdd * (0.1 * collisionNum)); 
     
 }
     
 }
 
 void InsuranceQuote::output(){
     
     total = costAdd;
     
     cout << nameF << " " << nameL << endl;
     
     
     if(company == 1){
         cout << "Company: Smercury" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Liability: $50.00" << endl;
         if(coverages[1] == 1)
             cout << "Collision: $50.00" << endl;
         if(coverages[2] == 1)
             cout << "Roadside Assistance: $20.00" << endl;
         if(coverages[3] == 1)
             cout << "Comprehensive: $15.00" << endl;
         if(coverages[4] == 1)
             cout << "Uninsured motorist: $10.00" << endl;
         if(coverages[5] == 1)
             cout << "Underinsured motorist: $10.00" << endl;
         if(coverages[6] == 1)
             cout << "Rental car: $10.00" << endl;
         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for penalites" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
         
     }
     
          
     
     if(company == 2){
         cout << "Company: Some State" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Liability: $45.00" << endl;
         if(coverages[1] == 1)
             cout << "Collision: $45.00" << endl;
         if(coverages[2] == 1)
             cout << "Roadside Assistance: $15.00" << endl;
         if(coverages[3] == 1)
             cout << "Comprehensive: $15.00" << endl;
         if(coverages[4] == 1)
             cout << "Rental car: $8.00" << endl;
         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for penalites" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
     }
       
     
     if(company == 3){
         cout << "Company: Ghetto Insurance" << endl;
         cout << "Coverages: " << endl;
         if(coverages[0] == 1)
             cout << "Liability: $25.00" << endl;
         if(coverages[1] == 1)
             cout << "Collision: $25.00" << endl;
         if(coverages[2] == 1)
             cout << "Roadside Assistance: $5.00" << endl;
         if(coverages[3] == 1)
             cout << "Comprehensive: $3.00" << endl;

         cout << "Subtotal: $" << fixed << setprecision(2) << costAccum << endl;
         cout << "After computing for penalites" << endl;
         cout << "Total: $" << fixed << setprecision(2) << total << endl;
     }
     
     
 }
 
    

