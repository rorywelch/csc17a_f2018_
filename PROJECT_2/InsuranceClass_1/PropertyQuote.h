

#ifndef PROPERTYQUOTE_H
#define PROPERTYQUOTE_H

#include "InsuranceQuote.h"
using namespace std;

class PropertyQuote :public InsuranceQuote {
public:
    PropertyQuote();
    PropertyQuote(const PropertyQuote& orig);
    virtual ~PropertyQuote();
    
    //Only use modified functions
    void setPolicies(); 
    void setTotal();
    void output();
    void setPersonal();
   
};

#endif /* BOATQUOTE_H */

