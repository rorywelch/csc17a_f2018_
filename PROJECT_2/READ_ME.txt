Functionality:
The first input is a choice of insurance type. This validation works, however since it is a 
char if you type in something like 12 it will be read as a 1, using the first input character.

The next input is company choice. Here I use exception handling which as far as I know works 
properly. However just like the the validation for insurance type it only reads the first
character typed in. So you could type in 25 and it will accept it as a 2. I was not sure how 
to get around that.

The next input is the policy choices. This has exception handling and functions just like the
company choice input with the same single character input limitation.

The next part is the personal information input. This works fine except for the age and the
at fault collisions can crash the program if you put in anything but a number. I tried 
exception handling but could not get it to work properly so I just left a basic while loop
for input validation.

The class output function works properly. However, the program calls the destructor before
my class is appended into my linked list so the final output at the end of the program
does not display the choosen policies but does show everything else. I don't know how to
keep the program from prematurely calling the destructor before appending the linked list.

My linked list is templatized. I did not use the insert or remove functions because they
didnt seem to fit the overall program in a logical or user friendly way. Aside from the
destructor problem I mentioned before, the linked list works properly. I included the .cpp
for the linked list but the whole file is commented out and not used.